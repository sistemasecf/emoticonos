package com.example.pruebaimagenes;

import java.util.ArrayList;

import com.example.pruebaimagenes.DataBaseHelper.Imagen;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

public class FavGrid extends BaseAdapter {
	private Context mContext;
	private SharedPreferences prefs;
	public Integer[] imagenes;
	private ArrayList<Imagen> Lista;
	

	// references to our images
	// en un array de Integer se guardan los números que son los id de todos los
	// recursos guardados en drawable
	// dentro de la carpeta res/drawable están todas las imágenes con esos
	// nombres. con su número entero se les puede identificar como recurso
//	public Integer[] perros = { R.drawable.gree01, R.drawable.gree02,
//			R.drawable.gree03, R.drawable.gree04, R.drawable.gree05,
//			R.drawable.gree06, R.drawable.gree07, R.drawable.gree08 };
//
//	public Integer[] premium = { R.drawable.fractal };
//
//	public Integer[] cosas = { R.drawable.ziggs0, R.drawable.ziggs1 };
//
//	public Integer[] gatos = { R.drawable.gato0 };
//	
//	public Integer[] todas= {R.drawable.gree01, R.drawable.gree02,
//			R.drawable.gree03, R.drawable.gree04, R.drawable.gree05,
//			R.drawable.gree06, R.drawable.gree07, R.drawable.gree08,R.drawable.fractal, R.drawable.ziggs0, R.drawable.ziggs1, R.drawable.gato0  };

	// el constructor necesita el contexto de la actividad donde se utiliza el
	// adapter
	public FavGrid(Context c, Integer[] imgs) {
		mContext = c;
		this.imagenes=imgs;
//		prefs = mContext.getSharedPreferences("PreferenciasImagenes",
//				Context.MODE_PRIVATE);
	
	}
	


	public int getCount() {// devuelve el número de elementos que se introducen
		// en el adapter
		
		
		
		return imagenes.length;
	}
	
	//public Integer[]   seleccion(){
//		Integer[]res=perros;
//		String palabra = prefs.getString("Op", "todas");
//		if(palabra.equals("perros"))res=perros;
//		if(palabra.equals("gatos"))res=gatos;
//		if(palabra.equals("premium"))res=premium;
//		if(palabra.equals("cosas"))res=cosas;
//		if(palabra.equals("todas"))res=todas;
//		return res;
//	}

	public Object getItem(int position) {
		// este método debería devolver el objeto que esta en esa posición del
		// adapter. No es necesario en este caso más que devolver un objeto
		// null.
		return null;
	}

	public long getItemId(int position) {
		// este método debería devolver el id de fila del item que esta en esa
		// posición del adapter. No es necesario en este caso más que devolver
		// 0.
		return 0;
	}

	// crear un nuevo ImageView para cada item referenciado por el Adapter
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// este método crea una nueva View para cada elemento añadido al
		// ImageAdapter. Se le pasa el View en el que se ha pulsado, converview
		// si convertview es null, se instancia y configura un ImageView con las
		// propiedades deseadas para la presentación de la imagen
		// si converview no es null, el ImageView local es inicializado con este
		// objeto View
		ImageView imageView;
		prefs = this.mContext.getSharedPreferences("PreferenciasImagenes",
				Context.MODE_PRIVATE);
		
		if (convertView == null) {

			imageView = new ImageView(mContext);
			imageView.setLayoutParams(new GridView.LayoutParams(144, 144));// ancho
			// y alto
			imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
			imageView.setPadding(1, 1, 1, 1);
		} else {
			imageView = (ImageView) convertView;
		}

		// la imagen de este imageView será el elemento de la
		// posicion 'position' del vector mThumbIds, declarado abajo.
		imageView.setImageResource(imagenes[position]);
		/*
         * 
         */

		// sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED,
		// Uri.parse("file://" + Environment.getExternalStorageDirectory())));

		return imageView;
	}



}

