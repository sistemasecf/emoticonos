package com.example.pruebaimagenes;


import java.io.File;
import java.io.FileOutputStream;

import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.EmbossMaskFilter;
import android.graphics.MaskFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;

public class paint extends Activity  implements ColorPickerDialog.OnColorChangedListener{

	
	MyView mv;
	AlertDialog dialog;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);

	mv= new MyView(this);
	mv.setDrawingCacheEnabled(true);
//	mv.setBackgroundResource();//set background here
	setContentView(mv);
	mPaint = new Paint();
	mPaint.setAntiAlias(true);
	mPaint.setDither(true);
	mPaint.setColor(0xF0000000);
	mPaint.setStyle(Paint.Style.STROKE);
	mPaint.setStrokeJoin(Paint.Join.ROUND);
	mPaint.setStrokeCap(Paint.Cap.ROUND);
	mPaint.setStrokeWidth(5);

	mEmboss = new EmbossMaskFilter(new float[] { 1, 1, 1 },
	                           0.4f, 6, 3.5f);
	mBlur = new BlurMaskFilter(8, BlurMaskFilter.Blur.NORMAL);
	}

	private Paint       mPaint;
	private MaskFilter  mEmboss;
	private MaskFilter  mBlur;

	public void colorChanged(int color) {
	mPaint.setColor(color);
	}

	public class MyView extends View {

	private static final float MINP = 0.25f;
	private static final float MAXP = 0.75f;
	private Bitmap  mBitmap;
	private Canvas  mCanvas;
	private Path    mPath;
	private Paint   mBitmapPaint;
	Context context;

	public MyView(Context c) {
	super(c);
	context=c;
	mPath = new Path();
	mBitmapPaint = new Paint(Paint.DITHER_FLAG);   
	//mBitmapPaint.setColor(0xFFFFFFFF);
	}

	@Override
	 protected void onSizeChanged(int w, int h, int oldw, int oldh) {
	super.onSizeChanged(w, h, oldw, oldh);
	Drawable d = getContext().getApplicationContext().getResources().getDrawable(R.drawable.fondo);  
	Bitmap fondo = drawableToBitmap(d);
	// con esto pinta mal pero devuelve fondo blanco habria que encontrar una solucion
//mBitmap = Bitmap.createBitmap(fondo, 100, 200, fondo.getWidth()-100, fondo.getHeight()-200);
//	mBitmap = Bitmap.createBitmap(icon, w, h, w, h);
	
	// con esto pinta bien pero devuelve un fondo negro
	mBitmap=	Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
	
	mCanvas = new Canvas(mBitmap);

	}

	@Override
	protected void onDraw(Canvas canvas) {
	super.onDraw(canvas);

	canvas.drawBitmap(mBitmap, 0, 0, mBitmapPaint);

	canvas.drawPath(mPath, mPaint);
	}

	private float mX, mY;
	private static final float TOUCH_TOLERANCE = 4;

	private void touch_start(float x, float y) {
	//showDialog(); 
	mPath.reset();
	mPath.moveTo(x, y);
	mX = x;
	mY = y;

	}
	private void touch_move(float x, float y) {
	float dx = Math.abs(x - mX);
	float dy = Math.abs(y - mY);
	if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
	    mPath.quadTo(mX, mY, (x + mX)/2, (y + mY)/2);
	    mX = x;
	    mY = y;
	}
	}
	private void touch_up() {
	mPath.lineTo(mX, mY);
	// commit the path to our offscreen
	mCanvas.drawPath(mPath, mPaint);
	// kill this so we don't double draw
	mPath.reset();
	mPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SCREEN));
	//mPaint.setMaskFilter(null);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
	float x = event.getX();
	float y = event.getY();

	switch (event.getAction()) {
	    case MotionEvent.ACTION_DOWN:
	        touch_start(x, y);
	        invalidate();
	        break;
	    case MotionEvent.ACTION_MOVE:

	        touch_move(x, y);
	        invalidate();
	        break;
	    case MotionEvent.ACTION_UP:
	        touch_up();
	        invalidate();
	        break;
	}
	return true;
	}  
	}

	private static final int COLOR_MENU_ID = Menu.FIRST;
	private static final int EMBOSS_MENU_ID = Menu.FIRST + 1;
	private static final int BLUR_MENU_ID = Menu.FIRST + 2;
	private static final int ERASE_MENU_ID = Menu.FIRST + 3;
	private static final int SRCATOP_MENU_ID = Menu.FIRST + 4;
	private static final int Save = Menu.FIRST + 5;
	private static final int STROKE = Menu.FIRST + 6;
	private static final int STROKEMED= Menu.FIRST + 7;
	private static final int STROKEBIG = Menu.FIRST + 8;

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	super.onCreateOptionsMenu(menu);

	menu.add(0, COLOR_MENU_ID, 0, "Color").setShortcut('3', 'c');
	menu.add(0, EMBOSS_MENU_ID, 0, "Emboss").setShortcut('4', 's');
	menu.add(0, BLUR_MENU_ID, 0, "Blur").setShortcut('5', 'z');
	menu.add(0, ERASE_MENU_ID, 0, "Erase").setShortcut('5', 'z');
	menu.add(0, SRCATOP_MENU_ID, 0, "SrcATop").setShortcut('5', 'z');
	menu.add(0, Save, 0, "Save and send").setShortcut('5', 'z');
	menu.add(0, STROKE, 0, "Stroke").setShortcut('6', 't');
	menu.add(0, STROKEMED, 0, "Stroke Med").setShortcut('7', 'm');
	menu.add(0, STROKEBIG, 0, "Stroke Big").setShortcut('8', 'b');

	return true;
	}

	@Override
	 public boolean onPrepareOptionsMenu(Menu menu) {
	 super.onPrepareOptionsMenu(menu);
	 return true;
	 }

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	mPaint.setXfermode(null);
	mPaint.setAlpha(0xFF);

	switch (item.getItemId()) {
	case COLOR_MENU_ID:
	    new ColorPickerDialog(this, this, mPaint.getColor()).show();
	    return true;
	case EMBOSS_MENU_ID:
	    if (mPaint.getMaskFilter() != mEmboss) {
	        mPaint.setMaskFilter(mEmboss);
	    } else {
	        mPaint.setMaskFilter(null);
	    }
	    return true;
	    
	case STROKE:
		mPaint.setStrokeWidth(5);
		return true;
	case STROKEMED:
		mPaint.setStrokeWidth(13);
		return true;
	case STROKEBIG:
		mPaint.setStrokeWidth(20);
		return true;
	case BLUR_MENU_ID:
	    if (mPaint.getMaskFilter() != mBlur) {
	        mPaint.setMaskFilter(mBlur);
	    } else {
	        mPaint.setMaskFilter(null);
	    }
	    return true;
	case ERASE_MENU_ID:
	    mPaint.setXfermode(new PorterDuffXfermode(  PorterDuff.Mode.CLEAR));
	    mPaint.setStrokeWidth(20);                                      
	    return true;
	case SRCATOP_MENU_ID:

	    mPaint.setXfermode(new PorterDuffXfermode(
	                                        PorterDuff.Mode.SRC_ATOP));
	    mPaint.setAlpha(0x80);
	    return true;
	case Save:
	    AlertDialog.Builder editalert = new AlertDialog.Builder(paint.this);
	        editalert.setTitle("Please Enter the name with which you want to Save");
	        final EditText input = new EditText(paint.this);
	        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
	                LinearLayout.LayoutParams.FILL_PARENT,
	                LinearLayout.LayoutParams.FILL_PARENT);
	        input.setLayoutParams(lp);
	        editalert.setView(input);
	        editalert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
	            public void onClick(DialogInterface dialog, int whichButton) {

	            String name= input.getText().toString();
	            Bitmap bitmap = mv.getDrawingCache();

	         String path = Environment.getExternalStorageDirectory().getAbsolutePath(); 
	            File file = new File("/sdcard/"+name+".png");           
	            try 
	            {
	                if(!file.exists())
	            {
	                file.createNewFile();
	            }
	                FileOutputStream ostream = new FileOutputStream(file);
	                Bitmap imageWithBG = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(),bitmap.getConfig());  // Create another image the same size
	                imageWithBG.eraseColor(Color.WHITE);  // set its background to white, or whatever color you want
	                Canvas canvas = new Canvas(imageWithBG);  // create a canvas to draw on the new image
	                canvas.drawBitmap(bitmap, 0f, 0f, null); // draw old image on the background
	                bitmap.recycle(); 
	                
	               // bitmap.compress(CompressFormat.JPEG, 100, ostream);
	                imageWithBG.compress(CompressFormat.JPEG, 100, ostream);
	                String filename = file.getAbsolutePath();
	                ostream.close();
	                MediaStore.Images.Media.insertImage(getContentResolver(),
	                		file.getAbsolutePath(), file.getName(), null);
	                Contenido.escaner(file.getAbsolutePath());
	                mv.invalidate();                            
	            } 
	            catch (Exception e) 
	            {
	                e.printStackTrace();
	            }finally
	            {

	                mv.setDrawingCacheEnabled(false);                           
	            }
	            }
	        });

	        editalert.show();       
	    return true;    
	 }
	 return super.onOptionsItemSelected(item);
	}
	
	
	
	public static Bitmap drawableToBitmap (Drawable drawable) {
	    if (drawable instanceof BitmapDrawable) {
	        return ((BitmapDrawable)drawable).getBitmap();
	    }

	    Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Config.ARGB_8888);
	    Canvas canvas = new Canvas(bitmap); 
	    drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
	    drawable.draw(canvas);

	    return bitmap;
	}
	
	}