package com.example.pruebaimagenes;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import android.app.Activity;
import android.app.TabActivity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class TodoContenido extends Activity{
	
	private GridView gridview;
	private SharedPreferences prefs;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.todo_el_contenido);
		prefs = this.getSharedPreferences("PreferenciasImagenes",
				Context.MODE_PRIVATE);

		

		
		gridview = (GridView) findViewById(R.id.gridtodo);// crear el
		// gridview a partir del elemento del xml gridview

		gridview.setAdapter(new ImageAdapter(this));// con setAdapter se llena
		// el gridview con datos. en
		// este caso un nuevo objeto de la clase ImageAdapter,
		// que está definida en otro archivo
		// para que detecte la pulsación se le añade un listener de itemClick
		// que recibe un OniTemClickListener creado con new

		gridview.setOnItemClickListener(new OnItemClickListener() {
			// dentro de este listener difinimos la función que se ejecuta al
			// hacer click en un item
			// el metodo pertenece a AdapterView, es decir, es
			// AdapterView.OnItemClickListener
			// dentro de este, tenemos el método onItemClick, que es el que se
			// invoca al pulsar un item del AdapterView
			// esa función recibe el objeto padre, que es un adapterview en el
			// que se ha pulasdo, una vista, que es el elemento sobre el que se
			// ha pulsado,
			// una posicion, que es la posicion del elemento dentro del adapter,
			// y un id, que es el id de fila del item que se ha pulsado
			@Override
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {
				// Toast para mostrar un mensaje. Escribe el nombre de tu clase
				// si no la llamaste MainActivity.
				// Al hacer click, esta mensaje muestra la posición
				/*
				 * String direccion = prefs.getString("vacio", "Imagen"); Uri
				 * uri; uri=Uri.parse(direccion); SharedPreferences.Editor
				 * editor = prefs.edit(); editor.putString("Imagen", "vacio");
				 * editor.commit();
				 */
				 final int pos=position;
				crearImagen(pos, getApplicationContext());
			}
		});


	}
	
	
	
	   public void crearImagen(int pos, Context context){
		   Drawable imagen;
			File folder = Environment.getExternalStorageDirectory();
			String fileName = folder.getPath() + "/" + "imag.jpg";
			File myFile = new File(fileName);
			if (myFile.exists())
				myFile.delete();

			/**
			 * 
			 * 
			 * PROBLEMA CON EL PERROS.POSITION solucionar el lunes
			 * 
			 * 
			 */
			try {
				OutputStream output = new FileOutputStream(Environment
						.getExternalStorageDirectory() + "/" + "imag.jpg");
				ImageAdapter imgAdpater = (ImageAdapter) gridview
						.getAdapter();
				imagen = (Drawable) getApplicationContext().getResources()
						.getDrawable(imgAdpater.getImagenes()[pos]);
				Bitmap bitmap = ((BitmapDrawable) imagen).getBitmap();
				ByteArrayOutputStream stream = new ByteArrayOutputStream();
				bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
				byte[] buffer = stream.toByteArray();
				// byte [] buffer=null;

				output.write(buffer);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			try {
				MediaStore.Images.Media.insertImage(getContentResolver(),
						fileName, "imag.jpg", null);

			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

				

				Contenido.escaner(fileName);
	    }
	
}
