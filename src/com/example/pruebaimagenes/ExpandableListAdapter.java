package com.example.pruebaimagenes;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

public class ExpandableListAdapter extends BaseExpandableListAdapter {
	 
    private Activity context;
    private Map<String, List<Integer>> laptopCollections;
    private List<String> laptops;

    
    public ExpandableListAdapter(Activity context, List<String> laptops,
            Map<String, List<Integer>> laptopCollections) {
        this.context = context;
        this.laptopCollections=null;
        this.laptopCollections = laptopCollections;
        this.laptops = laptops;
    }
 
    public Object getChild(int groupPosition, int childPosition) {
        return laptopCollections.get(laptops.get(groupPosition)).get(childPosition);
    	//return laptopCollections.get(laptops.get(groupPosition)).size();
    }
 
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }
 
    public View getChildView(final int groupPosition, final int childPosition,
            boolean isLastChild, View convertView, ViewGroup parent) {
        final int size = (Integer) getChild(groupPosition, childPosition);
        LayoutInflater inflater = context.getLayoutInflater();
        
        int tam=this.laptopCollections.get(laptops.get(groupPosition)).size();
        int filas=tam/5;
        int resto=tam%5;
        int posicion=childPosition*5;
        
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.child_item, null);
        }
 
        int longitud = size;
        
        filas=filas-childPosition;
        if(filas>0){
        	 
        	 ImageView item1 = (ImageView) convertView.findViewById(R.id.imagen1);
        	 item1.setImageResource((Integer) getChild(groupPosition, posicion));
        	 final int pos=posicion;
        	 item1.setOnClickListener(new OnClickListener() {
        		 
                 public void onClick(View v) {
                     AlertDialog.Builder builder = new AlertDialog.Builder(context);
                     builder.setMessage("¿Quieres enviar la imagen?");
                     builder.setCancelable(false);
                     builder.setPositiveButton("Si",
                             new DialogInterface.OnClickListener() {
                                 public void onClick(DialogInterface dialog, int id) {
                                	
                                	 //*********
                                	 crearImagen(groupPosition,pos);

                                 
                                 }
                             });
                     builder.setNegativeButton("No",
                             new DialogInterface.OnClickListener() {
                                 public void onClick(DialogInterface dialog, int id) {
                                     dialog.cancel();
                                 }
                             });
                     AlertDialog alertDialog = builder.create();
                     alertDialog.show();
                 }
             });
             ImageView item2 = (ImageView) convertView.findViewById(R.id.imagen2);
             item2.setImageResource((Integer) getChild(groupPosition, posicion+1));
 item2.setOnClickListener(new OnClickListener() {
        		 
                 public void onClick(View v) {
                     AlertDialog.Builder builder = new AlertDialog.Builder(context);
                     builder.setMessage("Do you want to remove?");
                     builder.setCancelable(false);
                     builder.setPositiveButton("Yes",
                             new DialogInterface.OnClickListener() {
                                 public void onClick(DialogInterface dialog, int id) {
                                    crearImagen(groupPosition, pos+1);
                                 }
                             });
                     builder.setNegativeButton("No",
                             new DialogInterface.OnClickListener() {
                                 public void onClick(DialogInterface dialog, int id) {
                                     dialog.cancel();
                                 }
                             });
                     AlertDialog alertDialog = builder.create();
                     alertDialog.show();
                 }
             });
             ImageView item3 = (ImageView) convertView.findViewById(R.id.imagen3);
             item3.setImageResource((Integer) getChild(groupPosition, posicion+2));
item3.setOnClickListener(new OnClickListener() {
        		 
                 public void onClick(View v) {
                     AlertDialog.Builder builder = new AlertDialog.Builder(context);
                     builder.setMessage("Do you want to remove?");
                     builder.setCancelable(false);
                     builder.setPositiveButton("Yes",
                             new DialogInterface.OnClickListener() {
                                 public void onClick(DialogInterface dialog, int id) {
                                	 crearImagen(groupPosition, pos+2);
                                 }
                             });
                     builder.setNegativeButton("No",
                             new DialogInterface.OnClickListener() {
                                 public void onClick(DialogInterface dialog, int id) {
                                     dialog.cancel();
                                 }
                             });
                     AlertDialog alertDialog = builder.create();
                     alertDialog.show();
                 }
             });
             ImageView item4 = (ImageView) convertView.findViewById(R.id.imagen4);
             item4.setImageResource((Integer) getChild(groupPosition, posicion+3));
item4.setOnClickListener(new OnClickListener() {
        		 
                 public void onClick(View v) {
                     AlertDialog.Builder builder = new AlertDialog.Builder(context);
                     builder.setMessage("Do you want to remove?");
                     builder.setCancelable(false);
                     builder.setPositiveButton("Yes",
                             new DialogInterface.OnClickListener() {
                                 public void onClick(DialogInterface dialog, int id) {
                                	 crearImagen(groupPosition, pos+3);
                                 }
                             });
                     builder.setNegativeButton("No",
                             new DialogInterface.OnClickListener() {
                                 public void onClick(DialogInterface dialog, int id) {
                                     dialog.cancel();
                                 }
                             });
                     AlertDialog alertDialog = builder.create();
                     alertDialog.show();
                 }
             });


ImageView item5 = (ImageView) convertView.findViewById(R.id.imagen5);
item5.setImageResource((Integer) getChild(groupPosition, posicion+4));
item5.setOnClickListener(new OnClickListener() {
	 
    public void onClick(View v) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Do you want to remove?");
        builder.setCancelable(false);
        builder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                   	 crearImagen(groupPosition, pos+4);
                    }
                });
        builder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
});
           //  return convertView;
             
        }
        if(filas==0){
        	if(resto==4){
            	ImageView item1 = (ImageView) convertView.findViewById(R.id.imagen1);
           	 item1.setImageResource((Integer) getChild(groupPosition, posicion));
           	 final int pos=posicion;
           	 item1.setOnClickListener(new OnClickListener() {
        		 
                 public void onClick(View v) {
                     AlertDialog.Builder builder = new AlertDialog.Builder(context);
                     builder.setMessage("¿Quieres enviar la imagen?");
                     builder.setCancelable(false);
                     builder.setPositiveButton("Si",
                             new DialogInterface.OnClickListener() {
                                 public void onClick(DialogInterface dialog, int id) {
                                	
                                	 //*********
                                	 crearImagen(groupPosition,pos);

                                 
                                 }
                             });
                     builder.setNegativeButton("No",
                             new DialogInterface.OnClickListener() {
                                 public void onClick(DialogInterface dialog, int id) {
                                     dialog.cancel();
                                 }
                             });
                     AlertDialog alertDialog = builder.create();
                     alertDialog.show();
                 }
             });
                ImageView item2 = (ImageView) convertView.findViewById(R.id.imagen2);
                item2.setImageResource((Integer) getChild(groupPosition, posicion+1));
                item2.setOnClickListener(new OnClickListener() {
           		 
                    public void onClick(View v) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setMessage("¿Quieres enviar la imagen?");
                        builder.setCancelable(false);
                        builder.setPositiveButton("Si",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                   	
                                   	 //*********
                                   	 crearImagen(groupPosition,pos+1);

                                    
                                    }
                                });
                        builder.setNegativeButton("No",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                        AlertDialog alertDialog = builder.create();
                        alertDialog.show();
                    }
                });
                
                ImageView item3 = (ImageView) convertView.findViewById(R.id.imagen3);
                item3.setImageResource((Integer) getChild(groupPosition, posicion+2));
                item3.setOnClickListener(new OnClickListener() {
           		 
                    public void onClick(View v) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setMessage("¿Quieres enviar la imagen?");
                        builder.setCancelable(false);
                        builder.setPositiveButton("Si",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                   	
                                   	 //*********
                                   	 crearImagen(groupPosition,pos+2);

                                    
                                    }
                                });
                        builder.setNegativeButton("No",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                        AlertDialog alertDialog = builder.create();
                        alertDialog.show();
                    }
                });
                
                ImageView item4 = (ImageView) convertView.findViewById(R.id.imagen4);
                item3.setImageResource((Integer) getChild(groupPosition, posicion+3));
                item3.setOnClickListener(new OnClickListener() {
           		 
                    public void onClick(View v) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setMessage("¿Quieres enviar la imagen?");
                        builder.setCancelable(false);
                        builder.setPositiveButton("Si",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                   	
                                   	 //*********
                                   	 crearImagen(groupPosition,pos+3);

                                    
                                    }
                                });
                        builder.setNegativeButton("No",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                        AlertDialog alertDialog = builder.create();
                        alertDialog.show();
                    }
                });
                
                ImageView item5 = (ImageView) convertView.findViewById(R.id.imagen5);
                item5.setImageResource(Color.TRANSPARENT);
            //    return convertView;
            	}
        	
        	
        	
        	if(resto==3){
        	ImageView item1 = (ImageView) convertView.findViewById(R.id.imagen1);
       	 item1.setImageResource((Integer) getChild(groupPosition, posicion));
       	 final int pos=posicion;
       	 item1.setOnClickListener(new OnClickListener() {
    		 
             public void onClick(View v) {
                 AlertDialog.Builder builder = new AlertDialog.Builder(context);
                 builder.setMessage("¿Quieres enviar la imagen?");
                 builder.setCancelable(false);
                 builder.setPositiveButton("Si",
                         new DialogInterface.OnClickListener() {
                             public void onClick(DialogInterface dialog, int id) {
                            	
                            	 //*********
                            	 crearImagen(groupPosition,pos);

                             
                             }
                         });
                 builder.setNegativeButton("No",
                         new DialogInterface.OnClickListener() {
                             public void onClick(DialogInterface dialog, int id) {
                                 dialog.cancel();
                             }
                         });
                 AlertDialog alertDialog = builder.create();
                 alertDialog.show();
             }
         });
            ImageView item2 = (ImageView) convertView.findViewById(R.id.imagen2);
            item2.setImageResource((Integer) getChild(groupPosition, posicion+1));
            item2.setOnClickListener(new OnClickListener() {
       		 
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setMessage("¿Quieres enviar la imagen?");
                    builder.setCancelable(false);
                    builder.setPositiveButton("Si",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                               	
                               	 //*********
                               	 crearImagen(groupPosition,pos+1);

                                
                                }
                            });
                    builder.setNegativeButton("No",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                }
            });
            
            ImageView item3 = (ImageView) convertView.findViewById(R.id.imagen3);
            item3.setImageResource((Integer) getChild(groupPosition, posicion+2));
            item3.setOnClickListener(new OnClickListener() {
       		 
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setMessage("¿Quieres enviar la imagen?");
                    builder.setCancelable(false);
                    builder.setPositiveButton("Si",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                               	
                               	 //*********
                               	 crearImagen(groupPosition,pos+2);

                                
                                }
                            });
                    builder.setNegativeButton("No",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                }
            });
            ImageView item4 = (ImageView) convertView.findViewById(R.id.imagen4);
            item4.setImageResource(Color.TRANSPARENT);
        //    return convertView;
        	}
        	if(resto==2){
            	ImageView item1 = (ImageView) convertView.findViewById(R.id.imagen1);
           	 item1.setImageResource((Integer) getChild(groupPosition, posicion));
           	 final int pos=posicion;
           	 item1.setOnClickListener(new OnClickListener() {
        		 
                 public void onClick(View v) {
                     AlertDialog.Builder builder = new AlertDialog.Builder(context);
                     builder.setMessage("¿Quieres enviar la imagen?");
                     builder.setCancelable(false);
                     builder.setPositiveButton("Si",
                             new DialogInterface.OnClickListener() {
                                 public void onClick(DialogInterface dialog, int id) {
                                	
                                	 //*********
                                	 crearImagen(groupPosition,pos);

                                 
                                 }
                             });
                     builder.setNegativeButton("No",
                             new DialogInterface.OnClickListener() {
                                 public void onClick(DialogInterface dialog, int id) {
                                     dialog.cancel();
                                 }
                             });
                     AlertDialog alertDialog = builder.create();
                     alertDialog.show();
                 }
             });
                ImageView item2 = (ImageView) convertView.findViewById(R.id.imagen2);
                item2.setImageResource((Integer) getChild(groupPosition, posicion+1));
                item2.setOnClickListener(new OnClickListener() {
           		 
                    public void onClick(View v) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setMessage("¿Quieres enviar la imagen?");
                        builder.setCancelable(false);
                        builder.setPositiveButton("Si",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                   	
                                   	 //*********
                                   	 crearImagen(groupPosition,pos+1);

                                    
                                    }
                                });
                        builder.setNegativeButton("No",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                        AlertDialog alertDialog = builder.create();
                        alertDialog.show();
                    }
                });
                ImageView item3 = (ImageView) convertView.findViewById(R.id.imagen3);
                ImageView item4 = (ImageView) convertView.findViewById(R.id.imagen4);
                ImageView item5 = (ImageView) convertView.findViewById(R.id.imagen5);
                item3.setImageResource(Color.TRANSPARENT);
                item4.setImageResource(Color.TRANSPARENT);
                item5.setImageResource(Color.TRANSPARENT);
           //     return convertView;	
        	}
        	if(resto==1){
            	ImageView item1 = (ImageView) convertView.findViewById(R.id.imagen1);
           	 item1.setImageResource((Integer) getChild(groupPosition, posicion));
           	 final int pos=posicion;
           	item1.setOnClickListener(new OnClickListener() {
          		 
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setMessage("¿Quieres enviar la imagen?");
                    builder.setCancelable(false);
                    builder.setPositiveButton("Si",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                               	
                               	 //*********
                               	 crearImagen(groupPosition,pos);

                                
                                }
                            });
                    builder.setNegativeButton("No",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                }
            });
           	ImageView item2 = (ImageView) convertView.findViewById(R.id.imagen2);
           	ImageView item3 = (ImageView) convertView.findViewById(R.id.imagen3);
            ImageView item4 = (ImageView) convertView.findViewById(R.id.imagen4);
            ImageView item5 = (ImageView) convertView.findViewById(R.id.imagen5);
            item2.setImageResource(Color.TRANSPARENT);
            item3.setImageResource(Color.TRANSPARENT);
            item4.setImageResource(Color.TRANSPARENT);
            item5.setImageResource(Color.TRANSPARENT);
               
        	}
        }
        
    
        
    	
        return convertView;	
    	
    }
 
    public int getChildrenCount(int groupPosition) {
    	
    	int filas= laptopCollections.get(laptops.get(groupPosition)).size()/5;
    	int resto =laptopCollections.get(laptops.get(groupPosition)).size()%5;
    	if(resto>0)resto=1;
       // return laptopCollections.get(laptops.get(groupPosition)).size();
        return filas+resto;
       
    }
 
    public Object getGroup(int groupPosition) {
        return laptops.get(groupPosition);
    }
 
    public int getGroupCount() {
        return laptops.size();
    }
 
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }
 
    public View getGroupView(int groupPosition, boolean isExpanded,
            View convertView, ViewGroup parent) {
        String laptopName = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.group_item,
                    null);
        }
        TextView item = (TextView) convertView.findViewById(R.id.nombre);
        item.setTypeface(null, Typeface.BOLD);
        item.setText(laptopName);
        ImageView Emot = (ImageView) convertView.findViewById(R.id.emoticono);
        Emot.setBackgroundResource(ImagenLista(laptopName));
        return convertView;
    }
    
    
    public int ImagenLista(String Nombre){
    	int imagen= 0;
    	if(Nombre.equals("Emoticonos"))imagen=R.drawable.blu;
    	if(Nombre.equals("Fractal"))imagen=R.drawable.fractal;
    	if(Nombre.equals("Premium"))imagen=R.drawable.ziggs0;
    	if(Nombre.equals("Gato"))imagen=R.drawable.gato0;
    	return imagen;
    }
 
    public boolean hasStableIds() {
        return true;
    }
 
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
    
    public void crearImagen(int groupPosition, int pos){
    	 Drawable imagen;
			File folder = Environment.getExternalStorageDirectory();
			String fileName = folder.getPath() + "/" + "imag.jpg";
			File myFile = new File(fileName);
			if (myFile.exists())
				myFile.delete();

			/**
			 * 
			 * 
			 * PROBLEMA CON EL PERROS.POSITION solucionar el lunes
			 * 
			 * 
			 */
			try {
				OutputStream output = new FileOutputStream(Environment
						.getExternalStorageDirectory() + "/" + "imag.jpg");
				
				imagen = (Drawable) context.getApplicationContext().getResources()
						.getDrawable((Integer) getChild(groupPosition, pos));
				int imag=(Integer) getChild(groupPosition, pos);
				DataBaseHelper myDB= new DataBaseHelper(context);
				myDB.InsertarImagen(imag);
				Bitmap bitmap = ((BitmapDrawable) imagen).getBitmap();
				ByteArrayOutputStream stream = new ByteArrayOutputStream();
				bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
				byte[] buffer = stream.toByteArray();
				// byte [] buffer=null;

				output.write(buffer);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			try {
				MediaStore.Images.Media.insertImage(context.getContentResolver(),
						fileName, "imag.jpg", null);

			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			Contenido.escaner(fileName);
    }
    
    
   
}
