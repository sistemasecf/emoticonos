package com.example.pruebaimagenes;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.TabActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TabHost;
import android.widget.Toast;
import android.widget.ExpandableListView.OnChildClickListener;

public class ListasActivity extends Activity{
	private SharedPreferences prefs;
	private String appName = "com.whatsapp";
	private Spinner cmbOpciones;
	
	   List<String> groupList;
	    List<Integer> childList;
	    Map<String, List<Integer>> imagenes;
	    ExpandableListView expListView;
	    
	    ImageView Emot;
	    ImageView frac;
	    ImageView Premi;
	    ImageView Gats;
	    
	    
	   public Integer[] emoticonos = { R.drawable.gree01, R.drawable.gree02,
    			R.drawable.gree03, R.drawable.gree04, R.drawable.gree05,
    			R.drawable.gree06, R.drawable.gree07, R.drawable.gree08, R.drawable.gree01 };

    	public Integer[] fractal = { R.drawable.fractal };

    	public Integer[] premium = { R.drawable.ziggs0, R.drawable.ziggs1 };

    	public Integer[] gatos = { R.drawable.gato0 };    

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.listas_activity);
		prefs = this.getSharedPreferences("PreferenciasImagenes",
				Context.MODE_PRIVATE);

		
		

createGroupList();

createCollection();

expListView = (ExpandableListView) findViewById(R.id.lista_iconos);
final ExpandableListAdapter expListAdapter = new ExpandableListAdapter(
        this, groupList, imagenes);
expListView.setAdapter(expListAdapter);

//setGroupIndicatorToRight();

expListView.setOnChildClickListener(new OnChildClickListener() {

    public boolean onChildClick(ExpandableListView parent, View v,
            int groupPosition, int childPosition, long id) {
        final String selected = (String) expListAdapter.getChild(
                groupPosition, childPosition);
        Toast.makeText(getBaseContext(), selected, Toast.LENGTH_LONG)
                .show();

        return true;
    }
});


	}
	
	

	 private void createGroupList() {
	        groupList = new ArrayList<String>();
	        groupList.add("Emoticonos");
	        groupList.add("Fractal");
	        groupList.add("Premium");
	        groupList.add("Gato");
	       
	    }
	 
	    private void createCollection() {
	    	
	    	
	    	
	    	
	    	imagenes=null;
	       imagenes = new LinkedHashMap<String, List<Integer>>();
	 
	        for (String laptop : groupList) {
	            if (laptop.equals("Emoticonos")) {
	                loadChild(emoticonos);
	               
	            } else if (laptop.equals("Fractal"))
	                loadChild(fractal);
	            else if (laptop.equals("Premium"))
	                loadChild(premium);
	            else if (laptop.equals("Gato"))
	                loadChild(gatos);
	           
	 
	            imagenes.put(laptop, childList);
	        }
	    }
	 
	    private void loadChild(Integer[] laptopModels) {
	        childList = new ArrayList<Integer>();
	        for (Integer model : laptopModels)
	            childList.add(model);
	    }
	 
	    private void setGroupIndicatorToRight() {
	        /* Get the screen width */
	        DisplayMetrics dm = new DisplayMetrics();
	        getWindowManager().getDefaultDisplay().getMetrics(dm);
	        int width = dm.widthPixels;
	 
	        expListView.setIndicatorBounds(width - getDipsFromPixel(35), width
	                - getDipsFromPixel(5));
	    }
	 
	    // Convert pixel to dip
	    public int getDipsFromPixel(float pixels) {
	        // Get the screen's density scale
	        final float scale = getResources().getDisplayMetrics().density;
	        // Convert the dps to pixels, based on density scale
	        return (int) (pixels * scale + 0.5f);
	    }




	public void mensajedecontenido() {
		DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
		    @Override
		    public void onClick(DialogInterface dialog, int which) {
		        switch (which){
		        case DialogInterface.BUTTON_POSITIVE:
		            //Yes button clicked
		        	startActivity(new Intent(Intent.ACTION_VIEW,
							Uri.parse("http://play.google.com/store/apps/details?id="
									+ appName)));
		            break;

		        case DialogInterface.BUTTON_NEGATIVE:
		            //No button clicked
		            break;
		        }
		    }
		};

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("¿Quieres comprar este paquete?").setPositiveButton("Si", dialogClickListener)
		    .setNegativeButton("No", dialogClickListener).show();
	}

	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.e("Result", " " + resultCode);
		switch (requestCode) {
		case 3:
			if (resultCode == Activity.RESULT_OK) {
				setResult(Activity.RESULT_OK, data);
				finish();
				break;
			}
		}
	}

}
