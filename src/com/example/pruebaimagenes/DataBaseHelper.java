package com.example.pruebaimagenes;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import static android.provider.BaseColumns._ID;

public class DataBaseHelper extends SQLiteOpenHelper{
	//private static String DB_PATH = "/data/data/com.example.pruebaimagenes/";
    private static String DB_NAME = "ImagenesDB";
    private SQLiteDatabase myDataBase;

    private final Context myContext;

    /**
     * Constructor
     *Save a reference to contect to acces the dir assets and take the database
     * @param contexto
     **/
    public DataBaseHelper(Context context) {
        super(context, DB_NAME, null, 1);
        this.myContext = context;
    }

  /**
   * Create a empty database and overwrite this with our database
   **/
    public void CreateDataBase() throws IOException{

        boolean dbExist = CheckDataBase();

        if(dbExist){
            //if exist dont do nothing
        }else{
        	//If not exist, create a new database in the folder of the aplication
            //later we overwrite the database
            this.getReadableDatabase();
            try {
                CopyDataBase();
            } catch (IOException e) {
                throw new Error("Error at copy Data Base");
            }
        }
    }

    /**
     * check the existence od the database
     * @return true if exist, else false
     **/
    private boolean CheckDataBase(){
        SQLiteDatabase checkDB = null;
        File Archivo=null;
        try{
            String myPath =myContext.getDatabasePath(DB_NAME).getPath();
            Archivo = myContext.getDatabasePath(DB_NAME);
            
            //checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
        }catch(SQLiteException e){
            //no exist
        }

//        if(checkDB != null){
//            checkDB.close();
//        }

        return Archivo.exists();
    }

    /**
     * Copy the new database overwriting the old database.
     * 
     **/
    private void CopyDataBase() throws IOException{

        //open database from folder assests
        InputStream myInput = myContext.getAssets().open(DB_NAME);

        //Destiny folder
        String outFileName = myContext.getDatabasePath(DB_NAME).getPath();

        //Open empty database
        OutputStream myOutput = new FileOutputStream(outFileName);

        //Copy database
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer))>0){
            myOutput.write(buffer, 0, length);
        }
        Log.e("DB", "Copiada DB");

        //close files
        myOutput.flush();
        myOutput.close();
        myInput.close();
    }

    /**
     * Open Database
     **/
    public void OpenDataBase() throws SQLException{
        String myPath = myContext.getDatabasePath(DB_NAME).getPath();
        myDataBase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);

    }

    /**
     * close Database
     **/
    @Override
    public synchronized void close() {
            if(myDataBase != null)
                myDataBase.close();

            super.close();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //not used
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //Not used
    }


    
    public class Imagen {
    	
    	int Imagen;
    	int Cantidad;
    	public Imagen(int Imagen, int Cantidad){
    		this.Imagen=Imagen;
    		this.Cantidad=Cantidad;
    	}
    }

    /*
     * Take all the words searched
     */
    
    
    
    
    
     public Integer[]  ObtenerFavoritos(){
        
    	 OpenDataBase();
        
        
        Cursor Imagenes = myDataBase.rawQuery("SELECT * FROM Imagenes ORDER BY repeticiones DESC", null);
       
       ArrayList<Imagen> Lista = new ArrayList<Imagen>();
       Integer[] imgs = null;
       ArrayList<Integer> ListAux=new ArrayList<Integer>();

     
        //Obtengo las imagenes
        Imagenes.moveToFirst();
         while (Imagenes.isAfterLast() == false) {
        	 Imagen img = new Imagen(Integer.parseInt(Imagenes.getString(1)),Integer.parseInt(Imagenes.getString(0)));
      
          
            Lista.add(img);
            Log.e("numeros de imagenes", Imagenes.getString(0)+ ", " + Imagenes.getString(1));
            Imagenes.moveToNext();
         }
         Imagenes.close();
         close();

//         
         for(Imagen object: Lista){
        	  ListAux.add(object.Imagen);
        	}

         imgs = new Integer[ListAux.size()];
         ListAux.toArray(imgs);
         return imgs;

     }
     
    
     
    public void InsertarImagen(int imagen){
    	int cantidad=0;
    	OpenDataBase();
    	Cursor ObtenerNumero = myDataBase.rawQuery("Select repeticiones from Imagenes where _id=" +imagen +";", null);
    	ObtenerNumero.moveToFirst();
    	if(ObtenerNumero.getCount()>0){
    	String num=ObtenerNumero.getString(0);
    	myDataBase.execSQL("Delete from Imagenes where _id="+imagen);
    	cantidad = Integer.parseInt(num);
    	}
    	ObtenerNumero.close();
    	cantidad++;
    	myDataBase.execSQL("Insert Into Imagenes (repeticiones,_id) VALUES  (" + cantidad + "," +imagen  + ")");
    	Cursor comprobar = myDataBase.rawQuery("Select repeticiones from Imagenes where _id=" +imagen +";", null);
    	comprobar.moveToFirst();
    	String num=comprobar.getString(0);
    	//Insertar.close();
    	close();
    }
     
     
     
     
     
}
