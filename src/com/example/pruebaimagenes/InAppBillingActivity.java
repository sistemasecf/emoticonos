package com.example.pruebaimagenes;

import com.imagenes.util.IabHelper;
import com.imagenes.util.IabResult;
import com.imagenes.util.Inventory;
import com.imagenes.util.Purchase;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;

public class InAppBillingActivity extends Activity {

	private static final String TAG = "com.example.inappbilling";
	IabHelper mHelper;
	static final String ITEM_SKU = "android.test.purchased";
	
	private Button clickButton;
	private Button buyButton;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_in_app_billing);

		buyButton = (Button)findViewById(R.id.buyButton);
		clickButton = (Button)findViewById(R.id.clickButton);
		clickButton.setEnabled(false);
		
		String base64EncodedPublicKey = "<your public key here>";
        
        	mHelper = new IabHelper(this, base64EncodedPublicKey);
        
        	mHelper.startSetup(new 
			IabHelper.OnIabSetupFinishedListener() {
				@Override
				public void onIabSetupFinished(IabResult result) {
					// TODO Auto-generated method stub
					  if (!result.isSuccess()) {
	        	           Log.d(TAG, "In-app Billing setup failed: " + 
						result);
	        	      } else {             
	        	      	    Log.d(TAG, "In-app Billing is set up OK");
			      }
				}
        	});
	} 
	
	IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener 
	= new IabHelper.OnIabPurchaseFinishedListener() {
	

	@Override
	public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
		// TODO Auto-generated method stub
		if (result.isFailure()) {
		      // Handle error
		      return;
		 }      
		 else if (purchase.getSku().equals(ITEM_SKU)) {
		     consumeItem();
		    buyButton.setEnabled(false);
		}	
	}
};


public void consumeItem() {
	mHelper.queryInventoryAsync(mReceivedInventoryListener);
}
	
IabHelper.QueryInventoryFinishedListener mReceivedInventoryListener 
   = new IabHelper.QueryInventoryFinishedListener() {


	@Override
	public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
		// TODO Auto-generated method stub
		if (result.isFailure()) {
			  // Handle failure
		      } else {
	                 mHelper.consumeAsync(inventory.getPurchase(ITEM_SKU), 
				mConsumeFinishedListener);
		      }	
	}
};


IabHelper.OnConsumeFinishedListener mConsumeFinishedListener =
new IabHelper.OnConsumeFinishedListener() {
 public void onConsumeFinished(Purchase purchase, 
       IabResult result) {

if (result.isSuccess()) {		    	 
 	  clickButton.setEnabled(true);
} else {
       // handle error
}
}
};


@Override
public void onDestroy() {
	super.onDestroy();
	if (mHelper != null) mHelper.dispose();
	mHelper = null;
}
	
	public void buttonClicked (View view)
	{
		clickButton.setEnabled(false);
		buyButton.setEnabled(true);
	}
	
	public void buyClick(View view) {
	     mHelper.launchPurchaseFlow(this, ITEM_SKU, 10001,   
			   mPurchaseFinishedListener, "");
}

}