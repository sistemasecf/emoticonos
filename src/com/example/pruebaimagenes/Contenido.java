package com.example.pruebaimagenes;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.TabActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.GridView;
import android.widget.Spinner;
import android.widget.TabHost;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class Contenido extends TabActivity {
	private SharedPreferences prefs;
	private static Activity activity;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.contenido);
		prefs = this.getSharedPreferences("PreferenciasImagenes",
				Context.MODE_PRIVATE);

		activity = this;
		
		DataBaseHelper myDB = new DataBaseHelper(this);
		try {
			myDB.CreateDataBase();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Resources res = getResources();

		TabHost tabs = (TabHost) findViewById(android.R.id.tabhost);
		tabs.setup();

		/**
		 * Genero las diferentes tabs
		 */
		
		TabHost.TabSpec spec = tabs.newTabSpec("Favoritos");
		spec.setContent(new Intent(this, Favoritos.class));
		spec.setIndicator("",
				res.getDrawable(android.R.drawable.btn_star_big_on));
		tabs.addTab(spec);

		spec = tabs.newTabSpec("Todos");
		spec.setContent(new Intent(this, TodoContenido.class));
		spec.setIndicator("",
				res.getDrawable(android.R.drawable.ic_menu_camera));
		tabs.addTab(spec);

		spec = tabs.newTabSpec("Desplegable");
		spec.setContent(new Intent(this, ListasActivity.class));
		spec.setIndicator("", res.getDrawable(android.R.drawable.btn_radio));
		tabs.addTab(spec);

		spec = tabs.newTabSpec("Lienzo");
		spec.setContent(new Intent(this, paint.class));
		spec.setIndicator("",
				res.getDrawable(R.drawable.pincel));
		tabs.addTab(spec);

		spec = tabs.newTabSpec("Premium");
		spec.setContent(new Intent(this, InAppBillingActivity.class));
		spec.setIndicator("",
				res.getDrawable(android.R.drawable.ic_menu_camera));
		tabs.addTab(spec);

		tabs.setCurrentTab(0);

	}

	
	
	/**
	 * Metodo para poder enviar imagenes por whatsapp
	 * @param fileName
	 */
	public static void escaner(String fileName) {
		MediaScannerConnection.scanFile(activity, new String[] { fileName },
				null, new MediaScannerConnection.OnScanCompletedListener() {
					public void onScanCompleted(String path, Uri uri) {
						Log.i("ExternalStorage", "Scanned " + path + ":");
						Log.i("ExternalStorage", "-> uri=" + uri);
						Intent shareIntent = new Intent(
								android.content.Intent.ACTION_SEND, uri);

						activity.setResult(RESULT_OK, shareIntent);

						activity.finish();

					}
				});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.e("Result", " " + resultCode);
		switch (requestCode) {
		case 3:
			if (resultCode == Activity.RESULT_OK) {
				setResult(Activity.RESULT_OK, data);
				finish();
				break;
			}
		}
	}

	@Override
	public void onDestroy() {
		activity = null;
		super.onDestroy();
	}

}
